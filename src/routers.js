import HomeComponent from "./components/pages/Home.vue"
import DetailsComponent from "./components/pages/CategoryDetails.vue"
import {createRouter, createWebHistory} from "vue-router"

const routes = [
{
    name:"HomeComponent",
    component:HomeComponent,
    path:"/"
},
{
    name:"DetailsComponent",
    component:DetailsComponent,
    path:"/DetailsComponent/:detailsId"
}
];

const router = createRouter({
    history:createWebHistory(),
    routes,
});
export default router;