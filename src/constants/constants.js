const GET_ALL_CATEGORY_LIST = "http://localhost:8000/categoryList";
const GET_ALL_CONTENT_LIST = "http://localhost:8000/contentList";
export default {
    GET_ALL_CATEGORY_LIST,
    GET_ALL_CONTENT_LIST,
}