
import { createApp } from 'vue'
import App from './App.vue'
import router from './routers'

import vueNumeralFilterInstaller from './number_formats';
import store from './store/store'
createApp(App).use(router).use(vueNumeralFilterInstaller).use(store).mount('#app')
