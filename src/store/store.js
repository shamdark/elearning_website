import api_endpoints from "../constants/constants"

import Axios from "axios"
import { createStore } from "vuex" 

const store = createStore({
    state:{
        category:[],
        content:[],
        cart:[],
        
       
    },

    getters:{
       categoryList(state){
           return state.category
       },
       contentList(state){
        return state.content
       },
       cartList(state){
        return state.cart
       },
      
    },

    actions:{
        getCategoryList(context){
           Axios.get(api_endpoints.GET_ALL_CATEGORY_LIST).then((response)=>{
              context.commit('categoryList',response.data.categoryList)
           })
        },

        getContentList(context){
            Axios.get(api_endpoints.GET_ALL_CONTENT_LIST).then((response)=>{
               context.commit('contentList',response.data.contentList)
            })
         },

         getCartList(context){
            // Axios.get('http://localhost:8000/contentList').then((response)=>{
            //    context.commit('cartList',response.data.cartList)
            // })
            if (!localStorage.getItem("cartInfo")) {
                localStorage.setItem("cartList", JSON.stringify([]));
              }
            let getCart = JSON.parse(localStorage.getItem("cartInfo"));
            context.commit('cartList',getCart)


         },
        
        
    },
    mutations:{
           categoryList(state,responseData){
               return state.category = responseData;
           },

           contentList(state,response){
             return state.content = response;
           },
           cartList(state,response){
             return state.cart = response;
           },

          
    }
});

export default store;